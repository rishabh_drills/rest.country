import React from "react";

import axios from 'axios'

import Country from "./countries";

import Dropdown from "./dropdown";
import Header from "./header";
import countryDetails from "./CountryDetails"

import {
  BrowserRouter as Router,
  Switch,
  Route,

} from "react-router-dom";

class App extends React.Component {
  state = {
    Countries: [],
    Search: "",
    filter: "",
    pageMount: true
  };

  componentDidMount(){
    axios.get("https://restcountries.com/v3.1/all")
    .then((res) => {
      this.setState({
        Countries: res.data
      })
    })
    return;
  }
 
  handleFilterInput = (filtered) => {
    this.setState({ filter: filtered });
  };

  handleSearch = (value) => {
    this.setState({ Search: value });
  };

  
  render() {
    return (

      <Router >
        <Switch>
        <Route exact path='/'>
        <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          padding: "2rem",
          backgroundColor: "hsl(0, 0%, 98%)",
        }}
      >
        <Header />
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            padding: "10px",
          }}
        >
          <Dropdown
            handleSearch={this.handleSearch}
            changes={this.handleFilterInput}
          />
        </div>

        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "center",
          }}
        >
          {this.state.Countries.filter((search) => {
            if (
              search.name.official
                .toLowerCase()
                .includes(this.state.Search.toLowerCase())
            ) {
              return true;
            } else if (this.state.Search === "") {
              return true;
            }
          })
            .filter((element) => {
              if (this.state.filter === element.region) {
                return true;
              } else if (this.state.filter === "") {
                return true;
              } else if (this.state.filter === "choose any region") {
                return true;
              }
            })
            .map((country) => {
              return <Country key={country["cca2"]} {...country} />;
            })}{" "}
          ;
        </div>
      </div>
          </Route>

          <Route path="/:code" component={countryDetails}>
          
          </Route>
        </Switch>
      </Router>
      
      
    );
  }
}

export default App;
