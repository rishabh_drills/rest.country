import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

class CountryDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Countries: "",
      url:undefined,
    };
  }

  componentDidMount() {
    const code = this.props.match.params.code;
    axios
      .get(`https://restcountries.com/v3.1/alpha/${code}`)
      .then((res) => {
        return res.data;
      })
      .then((data) => {
        this.setState({
          Countries: data[0],
          url: code,
        });
      });
  }

  componentDidUpdate(prevProps, prevState) {
    const code = this.props.match.params.code;
    if(prevState.url !== code){
      axios.get(`https://restcountries.com/v3.1/alpha/${code}`).then((res) => {
      this.setState({
        Countries: res.data[0],
        url: code
      });
    });
    }
    // const code = this.props.match.params.code;
    
  }

  render() {
    if (Object.keys(this.state.Countries).length !== 0) {
      const {flags,name,population,region,subregion,capital,tld,currencies,languages,borders,} = this.state.Countries;

      if (this.state.Countries === "")
      {
        return <h1>Loading...</h1>
      }
      else
      {
        return (
          <div>
            <div style={{padding:'5%', marginLeft:"3.5rem"}}>
              <Link to={"/"}>
                <button key={"back"}>Back</button>
              </Link>
            </div>
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                flexWrap: "wrap",
              }}
            >
              <div style={{ padding: "10%", display: "flex" }}>
                <img
                  style={{ width: "50%" }}
                  src={flags.svg}
                  alt={name.official}
                ></img>

                <ul>
                  <h3>{name.official}</h3>
                  <li>population :{population}</li>
                  <li>Region: {region}</li>
                  <li>Subregion: {subregion}</li>
                  <li>Capital: {capital}</li>
                  <li>Top level domain: {tld}</li>
                  <li>Languages: {languages.kal}</li>
                  <li>Currencies: {currencies.DKK}</li>
                  <h3>Border Countries</h3>

                  {borders !== undefined
                    ? borders.map((mappingBorder) => {

                        return (

                          <Link  to={`/${mappingBorder}` }>
                            <button key={mappingBorder} onClick={(()=>{
                              this.setState({url:mappingBorder})})}
                              >{mappingBorder}</button>
                          </Link>
                        );
                      })
                    : "no border countries"}
                </ul>
              </div>
            </div>
          </div>
        );
                    }
    }
  }
}

export default CountryDetails;
