import React from "react";
import {
    Link
  } from "react-router-dom";

const Country = (props) => {
  // console.log(props)
  return (
    <Link 
      style={{
        width: "15%",
        color: "black",
        backgroundColor: "white",
        margin: "3rem",
        display: "flex",
        flexDirection: "column-reverse",
        justifyContent: "center",
      }}

      to={`/${props.cca2}`}
    >
      <p>Capital: {props.capital}</p>
      <p>Region: {props.region}</p>
      <p>Population: {JSON.stringify(props.population)}</p>
      <h3>{props.name.official}</h3>
      <div>
        <img src={props.flags.svg} width="100%" alt="" />
      </div>
    </Link>
  );
};

export default Country;
