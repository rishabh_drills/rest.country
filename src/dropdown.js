import React from "react";

class Dropdown extends React.Component {
  render() {
    return (
      <React.Fragment>
        <select
          style={{
            width: "20%",
            alignSelf: "flex-end",
            padding: "1rem",
            marginRight: "10%",
          }}
          className="filter"
          type="text"
          onChange={(e) => this.props.changes(e.target.value)}
        >
          <option>choose any region</option>
          <option>Americas</option>
          <option>Europe</option>
          <option>Africa</option>
          <option>Asia</option>
          <option>Oceania</option>
          <option>Antarctic</option>
        </select>
        <input
          style={{
            marginLeft: "10%",
            width: "30%",
            alignSelf: "flex-start",
            marginTop: "-3rem",
            padding: "1rem",
          }}
          type="search"
          placeholder="Search for a Country..."
          onChange={(e) => this.props.handleSearch(e.target.value)}
        ></input>
      </React.Fragment>
    );
  }
}
export default Dropdown;
