import React from "react";

class Header extends React.Component {
  render() {
    return (
      <div
        style={{
          paddingLeft: "10%",
          paddingRight: "10%",
          display: "flex",
          justifyContent: "space-between",
          backgroundColor: "white",
        }}
      >
        <h1>Where in the world?</h1>
        <div style={{ padding: "10px", alignSelf: "center" }}>
          <button type="button">Mode</button>
        </div>
      </div>
    );
  }
}

export default Header;
